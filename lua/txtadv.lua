--Libraries
local socket=require("socket")
local lume=require("lume")
--Globals
local server={
 socket=assert(socket.bind("*",7890)),
 players={},--sample player:Nil={name="Nil",room="TowerTop",inventory={"Item1"},conn=nil}
 rooms={Tower1Top={name="Tower1Top",
         descrip="The room is quite cold, with a hole in the wall to the north and a stairway downwards to the south. Looking out of the hole, you see a long fall to the ground.",
         exits={south="Tower1Mid",north="Death"},
         players={},
         items={"Red Flag"},
         ec=false},
        Tower1Mid={name="Tower1Mid",
         descrip="The room is slightly chilly. The stone brick walls are is a state of slight disrepair, but the stairs upward to the north and down to the south are still functional.",
         exits={north="Tower1Grn",south="Tower1Top"},
         players={},
         items={},
         ec=false},
        Tower1Grn={name="Tower1Grn",
         descrip="The crackling fire provides warmth for the room. However, the rest of the room suggests no one has lived here in a long time. Nevertheless, there is a staircase higher into the tower to the north and a door to west.",
         exits={north="Tower1Mid",west="Court"},
         players={},
         items={},
         ec=false},
        Court={name="Court",
         descrip="The open courtyard is surrounded by high walls that protect from the strong icy winds. To the north, a small collection of buildings are present. To the east and west, there are two towers. To the south, there is a large staircase downwards.",
         exits={east="Tower1Grn",west="Tower2Grn",north="Town",south="PrisonOffice"},
         players={},
         items={},
         ec=false},
        Tower2Grn={name="Tower2Grn",
         descrip="The icy walls provide little comfort from the cold. While a fireplace is present, it is clear that no one has used it in ages, and a small puddle has formed in the ashes. A staircase to the north leads down, a door the east provides an exit, and a staircase to the south leads up. However, the southern staircase has collapsed, rendering it unusable.",
         exits={east="Court",north="Tower2Bsm"},
         players={},
         items={},
         ec=false},
        Tower2Bsm={name="Tower2Bsm",
         descrip="A torch on the wall provides both light and heat, and the crackle of the fire provides great comfort. A staircase to the north leads to the ground floor. This place used to be storage, but most of the shelves are now empty. However, if you look closely, you still might find something.",
         exits={north="Tower2Grn"},
         players={},
         items={"Chess Set"},
         ec=false},
        Town={name="Town",
         descrip="The surrounding buildings include a guard tower to the west, a locked door to the north, and a locked door to the east. If you head south, you could return to the court.",
         exits={south="Court",west="GuardTower"},
         players={},
         items={"Maze Cube"},
         ec=false},
        GuardTower={name="GuardTower",
         descrip="The view shows an endless icy plain. While the lack of walls let in the wind, a small fire in the center of the room provides warmth. There is a staircase downward to the east, and it seems that if you walked west across the wall you could reach the door into the western tower.",
         exits={east="Town",west="Tower2Mid"},
         players={},
         items={},
         ec=false},
        Tower2Mid={name="Tower2Mid",
         descrip="The chill of the ice in this room is uncomfortable, but is better than the winds outside. To the east is an exit. The southern staircase leads down, but there is little chance of getting back up again. The northern staircase, however, seems intact, and could be climbed.",
         exits={south="Tower2Grn",east="GuardTower",north="Tower2Top"},
         players={},
         items={},
         ec=false},
        Tower2Top={name="Tower2Top",
         descrip="A fireplace contains a small fire, warming the room slightly. However, the cold coming down from the southern staircase chills the room immensly. The northern staircase leads downward.",
         exits={north="Tower2Mid",south="Tower2Rof"},
         players={},
         items={},
         ec=false},
        Tower2Rof={name="Tower2Rof",
         descrip="The freezing cold chills you to the bone. You wish you had a proper coat. Little is here except a staircase leading down to the south.",
         exits={south="Tower2Top"},
         players={},
         items={"Blue Flag"},
         ec=false},
        PrisonOffice={name="PrisonOffice",
         descrip="Unlike the rest of the castle, this room still is warm. The torches on the walls provide lighting, showing the large desk in the center of the room. There is a staircase into the prison to the south and a staircase to the courtyard to the north.",
         exits={north="Court",south="PrisonStairs1"},
         players={},
         items={},
         ec=false},
        PrisonStairs1={name="PrisonStairs1",
         descrip="This is the first level of the prison. A few cages surround the room. There is a staircase to the south leading up and a staircase to the north leading down.",
         exits={south="PrisonOffice",north="PrisonStairs2"},
         players={},
         items={},
         ec=false},
        PrisonStairs2={name="PrisonStairs2",
         descrip="This is the second level of the prison. A few cages surround the room. There is a staircase to the north leading up and a staircase to the south leading down.",
         exits={south="PrisonStairs3",north="PrisonStairs1"},
         players={},
         items={},
         ec=false},
        PrisonStairs3={name="PrisonStairs3",
         descrip="This is the third level of the prison. A few cages surround the room. There is a staircase to the south leading up and a staircase to the north leading down.",
         exits={south="PrisonStairs2",north="PrisonStairs4"},
         players={},
         items={},
         ec=false},
        PrisonStairs4={name="PrisonStairs4",
         descrip="This is the fourth level of the prison. A few cages surround the room. There is a staircase to the north leading up and a staircase to the south leading down.",
         exits={south="PrisonStairs5",north="PrisonStairs3"},
         players={},
         items={},
         ec=false},
         PrisonStairs5={name="PrisonStairs5",
         descrip="This is the fifth level of the prison. A few cages surround the room. There is a staircase to the south leading up and a staircase to the north leading down.",
         exits={south="PrisonStairs4",north="PrisonDungeon"},
         players={},
         items={},
         ec=false},
        PrisonDungeon={name="PrisonDungeon",
         descrip="Despite the depth, the room is still chilly. Empty cages fill the room. A staircase to the north leads up and out. There is a deep pit to the west.",
         exits={north="PrisonStairs5",west="Death"},
         players={},
         items={},
         ec=false},
        Death={name="Death",
         descrip="You died. Game Over! Please Quit.",
         exits={},
         players={},
         items={},
         ec=false},--
        Tower1Top_A={name="Tower1Top_A",--Alternate Castle
         descrip="The geometry of the area seems impossible. Nevertheless, the highest room of the eastern tower seems reflected in the area. Four exits each lead to different areas, and it seems that if you try hard enough, you could return back to the eastern tower's top room.",
         exits={maze="Tower1Top", north="", south="", east="", west=""},
         players={},
         items={"Maroon Flag"},
         ec=true},
        Tower1Mid_A={name="Tower1Mid_A",
         descrip="Despite the chaos of the realm, it seems this room is just the emtpty middle room of the eastern tower, but slightly off. You see strange hallways leading in each of the four cardinal directions, and if you concentrate, you could return to the normal tower.",
         exits={maze="Tower1Mid", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
        Tower1Grn_A={name="Tower1Grn_A",
         descrip="Despite the flame that seems to be burning the whole room, the temperature is perfectly comfortable. The four exit seem safe enough, and the ground floor of the eastern tower seems as if it is right here, and easy to reach.",
         exits={maze="Tower1Grn", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
        Court_A={name="Court_A",
         descrip="The sky show an endless pattern of chaos, in red and white and green, and the area seems perfectly warm. The reflection of the courtyard is visible, and four archways lead to elsewhere.",
         exits={maze="Court", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
        Tower2Grn_A={name="Tower2Grn_A",
         descrip="The ice on the walls is a strage glowing pink. A large fire is present in the fireplace, and the southern broken staircase has steps floating in the air. The reflection of ground floor of the western tower additionaly has a door to the west.",
         exits={maze="Tower2Grn", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
        Tower2Bsm_A={name="Tower2Bsm_A",
         descrip="The boxes float in the air in this seemingly infinite storge area. The four archways present, this seems as if it is the ideal of the storage space in the western tower.",
         exits={maze="Tower2Bsm", north="", south="", east="", west=""},
         players={},
         items={"Fairy Chess Set"},
         ec=true},
        Town_A={name="Town_A",
         descrip="The chaotic assembly of buildings forms a cylinder leading upwards to the sky. Four archways lead to other rooms in the strange realm, but it seems if you just stepped perpendicular to this reality, you could go back to the town in the normal reality.",
         exits={maze="Town", north="", south="", east="", west=""},
         players={},
         items={"Cube of Chaos"},
         ec=true},
        GuardTower_A={name="GuardTower_A",
         descrip="A seemingly endless sea of chaos surronds the tower. Four archways lead to other sections of this world, but when you close your eyes you can hear the whistling wind from the guard tower...",
         exits={maze="GuardTower", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
        Tower2Mid_A={name="Tower2Mid_A",
         descrip="At first glance, you seem to be in the middle of the western tower, but the four archways in each direction suggest that you are somewhere... else.",
         exits={maze="Tower2Mid", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
        Tower2Top_A={name="Tower2Top_A",
         descrip="The empty fireplace hardly matters in this warm realm. Despite superficial resembelence to the highest room in the western tower, the four archways show that wherever you are, it is not the same.",
         exits={maze="Tower2Top", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
        Tower2Rof_A={name="Tower2Rof_A",
         descrip="When you look off of the edge of the tower, it seems that the tower is all that exsists here, and the sky surrounds you. The western tower's roof seems to be right next to you in a way you can't quite place, though. Four archways lead to other areas.",
         exits={maze="Tower2Rof", north="", south="", east="", west=""},
         players={},
         items={"Azure Flag"},
         ec=true},
        PrisonOffice_A={name="PrisonOffice_A",
         descrip="A desk covered in papers is in front of a giant filing system. A mirror on the wall shows the prison's office. Four archways lead to the rest of this arcane land.",
         exits={maze="PrisonOffice", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
        PrisonStairs1_A={name="PrisonStairs1_A",
         descrip="This is the first level of the prison. A few cages surround the room. There is a staircase to the south leading up and a staircase to the north leading down.",
         exits={maze="PrisonStairs1", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
        PrisonStairs2_A={name="PrisonStairs2_A",
         descrip="This is the second level of the prison. A few cages surround the room. There is a staircase to the north leading up and a staircase to the south leading down.",
         exits={maze="PrisonStairs2", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
        PrisonStairs3_A={name="PrisonStairs3_A",
         descrip="This is the third level of the prison. A few cages surround the room. There is a staircase to the south leading up and a staircase to the north leading down.",
         exits={maze="PrisonStairs3", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
        PrisonStairs4_A={name="PrisonStairs4_A",
         descrip="This is the fourth level of the prison. A few cages surround the room. There is a staircase to the north leading up and a staircase to the south leading down.",
         exits={maze="PrisonStairs4", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
         PrisonStairs5_A={name="PrisonStairs5_A",
         descrip="This is the fifth level of the prison. A few cages surround the room. There is a staircase to the south leading up and a staircase to the north leading down.",
         exits={maze="PrisonStairs5", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
        PrisonDungeon_A={name="PrisonDungeon_A",
         descrip="Despite the depth, the room is still chilly. Empty cages fill the room. A staircase to the north leads up and out. There is a deep pit to the west.",
         exits={maze="PrisonDungeon", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true},
        Death_A={name="Death_A",
         descrip="You died. Game Over! Please Quit.",
         exits={maze="Death", north="", south="", east="", west=""},
         players={},
         items={},
         ec=true}
        },
}
for i,j in pairs(server.rooms) do
  assert(j.players)
 for x,y in pairs(j.exits) do
  if y=="" then
   local keys=lume.filter(server.rooms, function (x) return x.ec end)
   local random=lume.randomchoice(keys)
   server.rooms[i].exits[x]=random.name
   --print("setting exit", j.name, x, random.name, assert(server.rooms[random.name]))
  else
   assert(server.rooms[y], "missing room in "..j.name..".")
  end
 end
end

--Functions
local function joins()
 server.socket:settimeout(0.1)
 local nc=server.socket:accept()
 if nc then
  for _,j in pairs(server.players) do
   j.conn:send("New player joining! No inputs will be checked until this player has finished.\n")
  end
  nc.settimeout(nc,10)
  nc:send("What will be your name?\n")
  local n,er=nc:receive()
  if er then
   nc:send("Too Late!\n") nc:close()
   for _,j in pairs(server.players) do
    j.conn:send("The new player failed to join.\n")
   end
   return
  end
  server.players[n]={name=n,room="Court",inventory={},conn=nc}
  table.insert(server.rooms.Court.players,n)
  nc:settimeout(0.01)
  for _,j in pairs(server.players) do
   j.conn:send(n.." joined.\n")
  end
  print(n .. " Connected")
 end
end

local cmds={}
 cmds.move=function (p,i)
  if i=="maze" and server.rooms[p.room].ec==false then
   if lume.any(p.inventory,function(x) return x=="Maze Cube" end) then
    local newroom={p.room,"_A"}
    for _,j in pairs(server.rooms[p.room].players) do
     server.players[j].conn:send(p.name.." left the room.\n")
    end
    lume.remove(server.rooms[p.room].players,p.name)
    p.room=table.concat(newroom)
    for _,j in pairs(server.rooms[p.room].players) do
     server.players[j].conn:send(p.name.." entered the room.\n")
    end
    table.insert(server.rooms[p.room].players,p.name)
    p.conn:send("\027[2J \027[0;0H")
    cmds.look(p)
    lume.remove(p.inventory,"Maze Cube")
    p.conn:send("You lost the Maze Cube.\n")
    local newcube=false
    while not newcube do
     --newcube=true
     local keys=lume.keys(server.rooms)
     local random=lume.randomchoice(keys)
     if server.rooms[random].ec==false then
      table.insert(server.rooms[random].items,"Maze Cube")
      newcube=true
     end
    end
    return
   end
  end
  if server.rooms[p.room].exits[i] then
   for n,j in pairs(server.rooms[p.room].players) do
    server.players[j].conn:send(p.name.." left the room.\n")
   end
   lume.remove(server.rooms[p.room].players,p.name)
   p.room=assert(server.rooms[p.room].exits[i])
   assert(server.rooms[p.room], p.room .. " does not exist")
   for n,j in pairs(server.rooms[p.room].players) do
    server.players[j].conn:send(p.name.." entered the room.\n")
   end
   table.insert(server.rooms[p.room].players,p.name)
   p.conn:send("\027[2J \027[0;0H")
   cmds.look(p)
  else
   p.conn:send("Invalid Input\n")
  end
 end
 cmds.look=function (p)
  p.conn:send(server.rooms[p.room].descrip .. "\n\n")
  local speak={}
  for n,j in pairs(server.rooms[p.room].players) do
   table.insert(speak,j)
  end
  p.conn:send("Other players in the room include "..table.concat(speak," and ")..".\n\n")
  speak={"nothing"}
  for n,j in pairs(server.rooms[p.room].items) do
   table.insert(speak,j)
  end
  if #speak>1 then
   lume.remove(speak,"nothing")
  end
  p.conn:send("Items in the room include "..table.concat(speak," and ")..".\n\n")
 end
 cmds.get=function (p,i)
  if lume.any(server.rooms[p.room].items,function(x) return x==i end) then
   table.insert(p.inventory,i)
   lume.remove(server.rooms[p.room].items,i)
   for n,j in pairs(server.rooms[p.room].players) do
    server.players[j].conn:send(p.name.." picked up "..i..".\n")
   end
  end
 end
 cmds.drop=function (p,i)
  if lume.any(p.inventory,function(x) return x==i end) then
   table.insert(server.rooms[p.room].items,i)
   lume.remove(p.inventory,i)
   for n,j in pairs(server.rooms[p.room].players) do
    server.players[j].conn:send(p.name.." dropped "..i..".\n")
   end
  end
 end
 cmds.inv=function (p)
  for i,j in pairs(p.inventory) do
   p.conn:send(j.."\n")
  end
 end
 cmds.say=function (p,i)
  for n,j in pairs(server.rooms[p.room].players) do
   server.players[j].conn:send(p.name.." says "..i.."\n")
  end
 end
 cmds.tag=function (p,i)
  for n,j in pairs(server.rooms[p.room].players) do
   if j==i then
    for x=#server.players[j].inventory,1,-1 do
     table.insert(server.rooms[p.room].items,server.players[j].inventory[x])
     table.remove(server.players[j].inventory,x)
    end
    lume.remove(server.rooms[server.players[j].room].players,j)
    server.players[j].room="PrisonDungeon"
    table.insert(server.rooms.PrisonDungeon.players,j)
    for _,k in pairs(server.players) do
     k.conn:send(p.name.." tagged "..j.."!\n")
    end
   end
  end
 end
 cmds.help=function (p)
  p.conn:send("Welcome to the castle! Commands are:  \nmove : Move north, south, east, or west. However, a certain item might let you go somewhere else...  \nlook : Look around the current room. Tells you the description, who is in the room, and what items there are.  \nget : Pick up an item.  \ndrop : Drops an item.  \ninv : Tells you what is in your inventory.  \nsay : Tells everyone in the same room what you want to say.  \ntag : Sends another player to the dungeon and drops their items.  \nhelp : Tells you what the commands do.")
 end

local function use(data,player)
 local x,y,cm,inp=string.find(data,"([a-z]+) ?(.*)")
 local c=cmds[cm] or function (p) p.conn:send("Invalid Command\n") end
 c(player,inp)
end

--Main Loop
while true do
 for name,p in pairs(server.players) do
  local n,er=p.conn.receive(p.conn)
  if n then print(p.name.." Sent "..n) use(n,p) end
  if er=="closed" then lume.remove(server.players, p) print(name.." Disconnected") end
 end
 joins()
end