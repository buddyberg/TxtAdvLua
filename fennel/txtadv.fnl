(local socket (require :socket))
(local fennel (require :fennel))
(local lume (require :lume))

(local server {:players {}
               :rooms {:Court {:descrip "The open courtyard is surrounded by high walls that protect from the strong icy winds. To the north, a small collection of buildings are present. To the east and west, there are two towers. To the south, there is a large staircase downwards."
                               :ec false
                               :exits {:east :Tower1Grn
                                       :north :Town
                                       :south :PrisonOffice
                                       :west :Tower2Grn}
                               :items {}
                               :name :Court
                               :players {}}
                       :Court_A {:descrip "The sky show an endless pattern of chaos, in red and white and green, and the area seems perfectly warm. The reflection of the courtyard is visible, and four archways lead to elsewhere."
                                 :ec true
                                 :exits {:east ""
                                         :maze :Court
                                         :north ""
                                         :south ""
                                         :west ""}
                                 :items {}
                                 :name :Court_A
                                 :players {}}
                       :Death {:descrip "You died. Game Over! Please Quit."
                               :ec false
                               :exits {}
                               :items {}
                               :name :Death
                               :players {}}
                       :Death_A {:descrip "You died. Game Over! Please Quit."
                                 :ec true
                                 :exits {:east ""
                                         :maze :Death
                                         :north ""
                                         :south ""
                                         :west ""}
                                 :items {}
                                 :name :Death_A
                                 :players {}}
                       :GuardTower {:descrip "The view shows an endless icy plain. While the lack of walls let in the wind, a small fire in the center of the room provides warmth. There is a staircase downward to the east, and it seems that if you walked west across the wall you could reach the door into the western tower."
                                    :ec false
                                    :exits {:east :Town :west :Tower2Mid}
                                    :items {}
                                    :name :GuardTower
                                    :players {}}
                       :GuardTower_A {:descrip "A seemingly endless sea of chaos surronds the tower. Four archways lead to other sections of this world, but when you close your eyes you can hear the whistling wind from the guard tower..."
                                      :ec true
                                      :exits {:east ""
                                              :maze :GuardTower
                                              :north ""
                                              :south ""
                                              :west ""}
                                      :items {}
                                      :name :GuardTower_A
                                      :players {}}
                       :PrisonDungeon {:descrip "Despite the depth, the room is still chilly. Empty cages fill the room. A staircase to the north leads up and out. There is a deep pit to the west."
                                       :ec false
                                       :exits {:north :PrisonStairs5
                                               :west :Death}
                                       :items {}
                                       :name :PrisonDungeon
                                       :players {}}
                       :PrisonDungeon_A {:descrip "Despite the depth, the room is still chilly. Empty cages fill the room. A staircase to the north leads up and out. There is a deep pit to the west."
                                         :ec true
                                         :exits {:east ""
                                                 :maze :PrisonDungeon
                                                 :north ""
                                                 :south ""
                                                 :west ""}
                                         :items {}
                                         :name :PrisonDungeon_A
                                         :players {}}
                       :PrisonOffice {:descrip "Unlike the rest of the castle, this room still is warm. The torches on the walls provide lighting, showing the large desk in the center of the room. There is a staircase into the prison to the south and a staircase to the courtyard to the north."
                                      :ec false
                                      :exits {:north :Court
                                              :south :PrisonStairs1}
                                      :items {}
                                      :name :PrisonOffice
                                      :players {}}
                       :PrisonOffice_A {:descrip "A desk covered in papers is in front of a giant filing system. A mirror on the wall shows the prison's office. Four archways lead to the rest of this arcane land."
                                        :ec true
                                        :exits {:east ""
                                                :maze :PrisonOffice
                                                :north ""
                                                :south ""
                                                :west ""}
                                        :items {}
                                        :name :PrisonOffice_A
                                        :players {}}
                       :PrisonStairs1 {:descrip "This is the first level of the prison. A few cages surround the room. There is a staircase to the south leading up and a staircase to the north leading down."
                                       :ec false
                                       :exits {:north :PrisonStairs2
                                               :south :PrisonOffice}
                                       :items {}
                                       :name :PrisonStairs1
                                       :players {}}
                       :PrisonStairs1_A {:descrip "This is the first level of the prison. A few cages surround the room. There is a staircase to the south leading up and a staircase to the north leading down."
                                         :ec true
                                         :exits {:east ""
                                                 :maze :PrisonStairs1
                                                 :north ""
                                                 :south ""
                                                 :west ""}
                                         :items {}
                                         :name :PrisonStairs1_A
                                         :players {}}
                       :PrisonStairs2 {:descrip "This is the second level of the prison. A few cages surround the room. There is a staircase to the north leading up and a staircase to the south leading down."
                                       :ec false
                                       :exits {:north :PrisonStairs1
                                               :south :PrisonStairs3}
                                       :items {}
                                       :name :PrisonStairs2
                                       :players {}}
                       :PrisonStairs2_A {:descrip "This is the second level of the prison. A few cages surround the room. There is a staircase to the north leading up and a staircase to the south leading down."
                                         :ec true
                                         :exits {:east ""
                                                 :maze :PrisonStairs2
                                                 :north ""
                                                 :south ""
                                                 :west ""}
                                         :items {}
                                         :name :PrisonStairs2_A
                                         :players {}}
                       :PrisonStairs3 {:descrip "This is the third level of the prison. A few cages surround the room. There is a staircase to the south leading up and a staircase to the north leading down."
                                       :ec false
                                       :exits {:north :PrisonStairs4
                                               :south :PrisonStairs2}
                                       :items {}
                                       :name :PrisonStairs3
                                       :players {}}
                       :PrisonStairs3_A {:descrip "This is the third level of the prison. A few cages surround the room. There is a staircase to the south leading up and a staircase to the north leading down."
                                         :ec true
                                         :exits {:east ""
                                                 :maze :PrisonStairs3
                                                 :north ""
                                                 :south ""
                                                 :west ""}
                                         :items {}
                                         :name :PrisonStairs3_A
                                         :players {}}
                       :PrisonStairs4 {:descrip "This is the fourth level of the prison. A few cages surround the room. There is a staircase to the north leading up and a staircase to the south leading down."
                                       :ec false
                                       :exits {:north :PrisonStairs3
                                               :south :PrisonStairs5}
                                       :items {}
                                       :name :PrisonStairs4
                                       :players {}}
                       :PrisonStairs4_A {:descrip "This is the fourth level of the prison. A few cages surround the room. There is a staircase to the north leading up and a staircase to the south leading down."
                                         :ec true
                                         :exits {:east ""
                                                 :maze :PrisonStairs4
                                                 :north ""
                                                 :south ""
                                                 :west ""}
                                         :items {}
                                         :name :PrisonStairs4_A
                                         :players {}}
                       :PrisonStairs5 {:descrip "This is the fifth level of the prison. A few cages surround the room. There is a staircase to the south leading up and a staircase to the north leading down."
                                       :ec false
                                       :exits {:north :PrisonDungeon
                                               :south :PrisonStairs4}
                                       :items {}
                                       :name :PrisonStairs5
                                       :players {}}
                       :PrisonStairs5_A {:descrip "This is the fifth level of the prison. A few cages surround the room. There is a staircase to the south leading up and a staircase to the north leading down."
                                         :ec true
                                         :exits {:east ""
                                                 :maze :PrisonStairs5
                                                 :north ""
                                                 :south ""
                                                 :west ""}
                                         :items {}
                                         :name :PrisonStairs5_A
                                         :players {}}
                       :Tower1Grn {:descrip "The crackling fire provides warmth for the room. However, the rest of the room suggests no one has lived here in a long time. Nevertheless, there is a staircase higher into the tower to the north and a door to west."
                                   :ec false
                                   :exits {:north :Tower1Mid :west :Court}
                                   :items {}
                                   :name :Tower1Grn
                                   :players {}}
                       :Tower1Grn_A {:descrip "Despite the flame that seems to be burning the whole room, the temperature is perfectly comfortable. The four exit seem safe enough, and the ground floor of the eastern tower seems as if it is right here, and easy to reach."
                                     :ec true
                                     :exits {:east ""
                                             :maze :Tower1Grn
                                             :north ""
                                             :south ""
                                             :west ""}
                                     :items {}
                                     :name :Tower1Grn_A
                                     :players {}}
                       :Tower1Mid {:descrip "The room is slightly chilly. The stone brick walls are is a state of slight disrepair, but the stairs upward to the north and down to the south are still functional."
                                   :ec false
                                   :exits {:north :Tower1Grn :south :Tower1Top}
                                   :items {}
                                   :name :Tower1Mid
                                   :players {}}
                       :Tower1Mid_A {:descrip "Despite the chaos of the realm, it seems this room is just the emtpty middle room of the eastern tower, but slightly off. You see strange hallways leading in each of the four cardinal directions, and if you concentrate, you could return to the normal tower."
                                     :ec true
                                     :exits {:east ""
                                             :maze :Tower1Mid
                                             :north ""
                                             :south ""
                                             :west ""}
                                     :items {}
                                     :name :Tower1Mid_A
                                     :players {}}
                       :Tower1Top {:descrip "The room is quite cold, with a hole in the wall to the north and a stairway downwards to the south. Looking out of the hole, you see a long fall to the ground."
                                   :ec false
                                   :exits {:north :Death :south :Tower1Mid}
                                   :items ["Red Flag"]
                                   :name :Tower1Top
                                   :players {}}
                       :Tower1Top_A {:descrip "The geometry of the area seems impossible. Nevertheless, the highest room of the eastern tower seems reflected in the area. Four exits each lead to different areas, and it seems that if you try hard enough, you could return back to the eastern tower's top room."
                                     :ec true
                                     :exits {:east ""
                                             :maze :Tower1Top
                                             :north ""
                                             :south ""
                                             :west ""}
                                     :items ["Maroon Flag"]
                                     :name :Tower1Top_A
                                     :players {}}
                       :Tower2Bsm {:descrip "A torch on the wall provides both light and heat, and the crackle of the fire provides great comfort. A staircase to the north leads to the ground floor. This place used to be storage, but most of the shelves are now empty. However, if you look closely, you still might find something."
                                   :ec false
                                   :exits {:north :Tower2Grn}
                                   :items ["Chess Set"]
                                   :name :Tower2Bsm
                                   :players {}}
                       :Tower2Bsm_A {:descrip "The boxes float in the air in this seemingly infinite storge area. The four archways present, this seems as if it is the ideal of the storage space in the western tower."
                                     :ec true
                                     :exits {:east ""
                                             :maze :Tower2Bsm
                                             :north ""
                                             :south ""
                                             :west ""}
                                     :items ["Fairy Chess Set"]
                                     :name :Tower2Bsm_A
                                     :players {}}
                       :Tower2Grn {:descrip "The icy walls provide little comfort from the cold. While a fireplace is present, it is clear that no one has used it in ages, and a small puddle has formed in the ashes. A staircase to the north leads down, a door the east provides an exit, and a staircase to the south leads up. However, the southern staircase has collapsed, rendering it unusable."
                                   :ec false
                                   :exits {:east :Court :north :Tower2Bsm}
                                   :items {}
                                   :name :Tower2Grn
                                   :players {}}
                       :Tower2Grn_A {:descrip "The ice on the walls is a strage glowing pink. A large fire is present in the fireplace, and the southern broken staircase has steps floating in the air. The reflection of ground floor of the western tower additionaly has a door to the west."
                                     :ec true
                                     :exits {:east ""
                                             :maze :Tower2Grn
                                             :north ""
                                             :south ""
                                             :west ""}
                                     :items {}
                                     :name :Tower2Grn_A
                                     :players {}}
                       :Tower2Mid {:descrip "The chill of the ice in this room is uncomfortable, but is better than the winds outside. To the east is an exit. The southern staircase leads down, but there is little chance of getting back up again. The northern staircase, however, seems intact, and could be climbed."
                                   :ec false
                                   :exits {:east :GuardTower
                                           :north :Tower2Top
                                           :south :Tower2Grn}
                                   :items {}
                                   :name :Tower2Mid
                                   :players {}}
                       :Tower2Mid_A {:descrip "At first glance, you seem to be in the middle of the western tower, but the four archways in each direction suggest that you are somewhere... else."
                                     :ec true
                                     :exits {:east ""
                                             :maze :Tower2Mid
                                             :north ""
                                             :south ""
                                             :west ""}
                                     :items {}
                                     :name :Tower2Mid_A
                                     :players {}}
                       :Tower2Rof {:descrip "The freezing cold chills you to the bone. You wish you had a proper coat. Little is here except a staircase leading down to the south."
                                   :ec false
                                   :exits {:south :Tower2Top}
                                   :items ["Blue Flag"]
                                   :name :Tower2Rof
                                   :players {}}
                       :Tower2Rof_A {:descrip "When you look off of the edge of the tower, it seems that the tower is all that exsists here, and the sky surrounds you. The western tower's roof seems to be right next to you in a way you can't quite place, though. Four archways lead to other areas."
                                     :ec true
                                     :exits {:east ""
                                             :maze :Tower2Rof
                                             :north ""
                                             :south ""
                                             :west ""}
                                     :items ["Azure Flag"]
                                     :name :Tower2Rof_A
                                     :players {}}
                       :Tower2Top {:descrip "A fireplace contains a small fire, warming the room slightly. However, the cold coming down from the southern staircase chills the room immensly. The northern staircase leads downward."
                                   :ec false
                                   :exits {:north :Tower2Mid :south :Tower2Rof}
                                   :items {}
                                   :name :Tower2Top
                                   :players {}}
                       :Tower2Top_A {:descrip "The empty fireplace hardly matters in this warm realm. Despite superficial resembelence to the highest room in the western tower, the four archways show that wherever you are, it is not the same."
                                     :ec true
                                     :exits {:east ""
                                             :maze :Tower2Top
                                             :north ""
                                             :south ""
                                             :west ""}
                                     :items {}
                                     :name :Tower2Top_A
                                     :players {}}
                       :Town {:descrip "The surrounding buildings include a guard tower to the west, a locked door to the north, and a locked door to the east. If you head south, you could return to the court."
                              :ec false
                              :exits {:south :Court :west :GuardTower}
                              :items ["Maze Cube"]
                              :name :Town
                              :players {}}
                       :Town_A {:descrip "The chaotic assembly of buildings forms a cylinder leading upwards to the sky. Four archways lead to other rooms in the strange realm, but it seems if you just stepped perpendicular to this reality, you could go back to the town in the normal reality."
                                :ec true
                                :exits {:east ""
                                        :maze :Town
                                        :north ""
                                        :south ""
                                        :west ""}
                                :items ["Cube of Chaos"]
                                :name :Town_A
                                :players {}}}
               :socket (assert (socket.bind "*" 7890))})

(each [i j (pairs server.rooms)]
  (each [x y (pairs j.exits)]
    (case y
     "" (let [filter (lume.filter server.rooms (fn [x] x.ec)) 
              random (lume.randomchoice filter)]
          (tset server.rooms i :exits x random.name)))))

(fn joins []
  (server.socket:settimeout 0.1)
  (case (server.socket:accept)
    a (do
    (print "New Player Connecting")
    (each [_ j (pairs server.players)]
      (j.conn:send "New player joining! No inputs will be checked until this player has finished.\n"))
    (a.settimeout a 10)
    (a:send "What will be your name?\n")
    (case [(a:receive)]
      [_b c] (do (a:send "Too Late!\n")
      (a:close)
      (each [_ j (pairs server.players)]
        (j.conn:send "The new player failed to join.\n")))
      [b _c] (do
      (tset server.players b {:conn a :inventory {} :name b :room :Court})
      (table.insert server.rooms.Court.players b)
      (a:settimeout 0.01)
      (each [_ j (pairs server.players)] (j.conn:send (.. b " joined.\n")))
      (print (.. b " Connected")))))))

(local cmds {})

(fn cmds.move [p i]
                 (case [i (. server.rooms p.room) (lume.any p.inventory (fn [x] (= x "Maze Cube")))]
                   [:maze {:ec false} true] 
                   (let [newroom [p.room :_A]]
                    (each [_ j (pairs (. server.rooms p.room :players))]
                      (: (. server.players j :conn) :send (.. p.name " left the room.
")))
                    (lume.remove (. server.rooms p.room :players) p.name)
                    (set p.room (table.concat newroom))
                    (each [_ j (pairs (. server.rooms p.room :players))]
                      (: (. server.players j :conn) :send (.. p.name " entered the room.
")))
                    (table.insert (. server.rooms p.room :players) p.name)
                    (p.conn:send "\027[2J \027[0;0H")
                    (cmds.look p)
                    (lume.remove p.inventory "Maze Cube")
                    (p.conn:send "You lost the Maze Cube.\n")
                    (let [castlerooms (lume.reject server.rooms :ec)]
                     (table.insert (. (lume.randomchoice castlerooms) :items) "Maze Cube")))
                   (where [_ room] (. room.exits i))
                   (do
                    (each [n j (pairs (. server.rooms p.room :players))]
                     (: (. server.players j :conn) :send (.. p.name " left the room.
")))
                    (lume.remove (. server.rooms p.room :players) p.name)
                    (set p.room (. server.rooms p.room :exits i))
                    (each [n j (pairs (. server.rooms p.room :players))]
                     (: (. server.players j :conn) :send (.. p.name " entered the room.
")))
                    (table.insert (. server.rooms p.room :players) p.name)
                    (p.conn:send "\027[2J \027[0;0H")
                    (cmds.look p)) 
                    _ (p.conn:send "Invalid Input.\n")))

(set cmds.look
     (fn [p]
       (p.conn:send (.. (. server.rooms p.room :descrip) "\n\n"))
       (local speak {})
       (each [n j (pairs (. server.rooms p.room :players))]
         (table.insert speak j))
       (p.conn:send (.. "Other players in the room include "
                        (table.concat speak " and ") ".\n\n"))
       (doto speak lume.clear (table.insert :nothing))
       (each [n j (pairs (. server.rooms p.room :items))]
         (table.insert speak j))
       (case speak
         [a b _] (lume.remove speak a))
       (p.conn:send (.. "Items in the room include "
                        (table.concat speak " and ") ".\n\n"))))

(set cmds.get (fn [p i]
                (case (. server.rooms p.room :items)
                  (where a (lume.find a i)) (do
                    (table.insert p.inventory i)
                    (lume.remove a i)
                    (each [n j (pairs (. server.rooms p.room :players))]
                      (: (. server.players j :conn) :send
                         (.. p.name " picked up " i ".\n")))))))

(set cmds.drop (fn [p i]
                 (case p.inventory
                  (where a (lume.find a i)) (do
                   (table.insert (. server.rooms p.room :items) i)
                   (lume.remove a i)
                   (each [n j (pairs (. server.rooms p.room :players))]
                     (: (. server.players j :conn) :send
                        (.. p.name " dropped " i ".\n")))))))

(set cmds.inv
     (fn [p]
       (each [i j (pairs p.inventory)] (p.conn:send (.. j "\n")))))

(set cmds.say (fn [p i]
                (each [n j (pairs (. server.rooms p.room :players))]
                  (: (. server.players j :conn) :send
                     (.. p.name " says " i "\n")))))

(set cmds.tag
     (fn [p i]
       (each [n j (pairs (. server.rooms p.room :players))]
         (case [j i]
           [a a] (do
           (for [x (length (. server.players j :inventory)) 1 (- 1)]
             (table.insert (. server.rooms p.room :items)
                           (. server.players j :inventory x))
             (table.remove (. server.players j :inventory) x))
           (lume.remove (. server.rooms (. server.players j :room) :players) j)
           (tset (. server.players j) :room :PrisonDungeon)
           (table.insert server.rooms.PrisonDungeon.players j)
           (each [_ k (pairs server.players)]
             (k.conn:send (.. p.name " tagged " j "!\n"))))))))

(set cmds.help (fn [p]
                 (p.conn:send "Welcome to the castle! Commands are:  
move : Move north, south, east, or west. However, a certain item might let you go somewhere else...  
look : Look around the current room. Tells you the description, who is in the room, and what items there are.  
get : Pick up an item.  
drop : Drops an item.  
inv : Tells you what is in your inventory.  
say : Tells everyone in the same room what you want to say.  
tag : Sends another player to the dungeon and drops their items.  
help : Tells you what the commands do.")))

(fn use [data player]
  (let [(x y cm inp) (string.find data "([a-z]+) ?(.*)")
        c (or (. cmds cm) (fn [p] (p.conn:send "Invalid Command\n")))]
    (c player inp)))

(while true
  (each [name p (pairs server.players)]
    (local (n er) (p.conn.receive p.conn))
    (case [n er]
      [a _b] (do (print (.. p.name " Sent " n)) (use n p))
      (where [_a b] (= b :closed)) (do (lume.remove (. server.rooms p.room :players) p.name) 
                                      (lume.remove server.players p)
                                      (print (.. name " Disconnected")))))
  (joins))

