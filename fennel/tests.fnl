(local faith (require :faith))
(local socket (require :socket))

(fn return [client a]
  (case [(client:receive)]
    [_ b] a
    [b _] (return client (.. a b))))

(fn test-move []
  (let [client (assert (socket.connect :localhost 7890))]
    (client:receive)
    (client:send "Test Move\n")
    (client:receive)
    (client:send "move north\n")
    (client:receive)
    (faith.= "\027[2J \027[0;0HThe surrounding buildings include a guard tower to the west, a locked door to the north, and a locked door to the east. If you head south, you could return to the court." (assert (client:receive)))
    (client:close)))

(fn test-help []
  (let [client (assert (socket.connect :localhost 7890))]
    (client:receive)
    (client:send "Test Help\n")
    (client:receive)
    (client:send "help\n")
    (faith.match "Commands are: " (assert (client:receive)))
    (faith.match "move : Move north, south, east, or west." (assert (client:receive)))
    (client:close)))
    
(fn test-inv []
  (let [client (assert (socket.connect :localhost 7890))]
    (client:settimeout 0.5)
    (client:receive)
    (client:send "Test Inv\n")
    (client:receive)
    (client:send "move north\n")
    (return client "")
    (client:send "get Maze Cube\n")
    (faith.match "picked up Maze Cube" (assert (client:receive)))
    (client:send "inv\n")
    (faith.match "Maze Cube" (assert (client:receive)))
    (client:send "drop Maze Cube\n")
    (faith.match "dropped Maze Cube" (assert (client:receive)))
    (client:close)))

{: test-move
 : test-help
 : test-inv}