type player = { name : string;
                mutable items : string list;
                mutable room : string;
                inp : in_channel; 
                out : out_channel; 
                fd : Unix.file_descr }

type room = { description : string;
              exits : (string, string) Hashtbl.t;
              players : (Unix.file_descr, player) Hashtbl.t;
              mutable items : string list; }

let dprint string = Printf.printf "%s\n%!" string
          
let joins server joiners =
  let (nc,_) = Unix.accept server in
  let _ = dprint "Someone connected." in
  let inp = Unix.in_channel_of_descr nc in
  let out = Unix.out_channel_of_descr nc in
  let _ = output_string out "What is your name? " in
  let _ = flush out in
  let newplayer = {name = "";
                   items = [];
                   room = "here";
                   inp = inp;
                   out = out;
                   fd = nc} in
  newplayer :: joiners
  
let jupdate server joiners players rooms=
  let rec rfunct list =
    match list with
    | a :: r -> (let n1 = rfunct r in
                 let read = (try input_line a.inp with
                             | End_of_file -> "DISCONNEXCTED"
                             | Sys_blocked_io -> "") in
                 let l1 = List.of_seq (Hashtbl.to_seq_values players) in
                 let l2 = List.map (fun a -> a.name) l1 in
                 match read with
                 (* TODO: duplicate names *)
                 | "" -> a :: n1
                 | "DISCONNEXCTED" -> let _ = dprint "Player quit" in n1
                 | u when List.exists (fun v -> v==u) l2 -> let _ = output_string a.out "Alreay in Use!\n" in a :: n1
                 | s -> let _ = dprint "Name Recieved" in
                        
                        let np = {a with name = s} in
                        let _ = Hashtbl.add players a.fd np in
                        let _ = Hashtbl.add (Hashtbl.find rooms "here").players a.fd np in n1)
    | [] -> []
  in rfunct joiners

let rec item_string items =
  match items with
  | [] -> ""
  | v :: r -> let pstring = item_string r in
              let nstring = v ^ "\n" ^ pstring in
              nstring


     
let pupdate server players rooms=
  List.iter (fun (key,value) ->
      let _ = dprint value.name in
      let croom = Hashtbl.find rooms value.room in
      let read = (try input_line value.inp with
                  | End_of_file -> let _ = Hashtbl.remove players key in ""
                  | Sys_blocked_io -> "") in
      let sread = String.split_on_char ' ' read in
      let _ = match sread with
      | ["look"] -> output_string value.out (croom.description^"\nThe room has the following items in it:\n"^(item_string croom.items))
      | ["move"; d] -> (match Hashtbl.find_opt croom.exits d with
                        | None -> output_string value.out "Invalid Direction\n"
                        | (Some a) -> (let nroom = Hashtbl.find rooms a in
                                       let () = Hashtbl.remove croom.players value.fd in
                                       let () = Hashtbl.add nroom.players value.fd value in
                                       let _ = value.room <- a in
                                       output_string value.out "Moved\n") )
      | "get" :: i :: [] -> (let valid = List.mem i croom.items in
                             match valid with
                             | true -> let nitems = List.filter (fun v -> not (v = i)) croom.items in
                                       let _ = croom.items <- nitems in
                                       let _ = value.items <- i :: value.items in
                                       output_string value.out ("Picked up "^i^"!\n")
                             | false -> output_string value.out "No such item!\n")
      | "make" :: i :: [] -> value.items <- i :: value.items
      | "drop" :: i :: [] -> (let valid = List.mem i value.items in
                              match valid with
                              | true -> let nitems = List.filter (fun v -> not (v = i)) value.items in
                                        let _ = value.items <- nitems in
                                        let _ = croom.items <- i :: croom.items in
                                        output_string value.out ("Dropped "^i^"!\n")
                              | false -> output_string value.out "You don't have that!\n")
      | "say" :: t :: [] -> (Hashtbl.iter (fun k2 v2 -> output_string v2.out (value.name^" said "^t^"\n")) croom.players)
      | "inv" :: [] -> List.iter (fun a -> output_string value.out (a^"\n")) value.items
      | [""] -> dprint "No Input!"
      | _ -> output_string value.out "Invalid Input!\n" in
      flush value.out
    ) (List.of_seq (Hashtbl.to_seq players))

let rec loop rooms server players joiners =
  let njoiners = try joins server joiners with
                   Unix.Unix_error (Unix.EAGAIN,_,_) -> joiners in
  let njoiners = jupdate server njoiners players rooms in
  let () = pupdate server players rooms in
  loop rooms server players njoiners

let () =
  let rooms = Hashtbl.create 1 in (*[{description = "Testing??";
                exits = Hashtbl.create 1;
                players = Hashtbl.create 1;
                items = []}
              ] in*)
  let _ = Hashtbl.add rooms "here" {description = "This place is called Here.\n\nHead south to go to There.\n";
                               exits = Hashtbl.of_seq (List.to_seq [("south","there");("west","here")]);
                               players = Hashtbl.create 1;
                               items = []} in
  let _ = Hashtbl.add rooms "there" {description = "This place is called There.\n\nHead north to go to Here.\n";
                                     exits = Hashtbl.of_seq (List.to_seq [("north","here");("south","there")]);
                                     players = Hashtbl.create 1;
                                     items = []} in
  let server = Unix.socket PF_INET SOCK_STREAM 0 in
  let players = Hashtbl.create 1 in
  let _ = Unix.bind server (Unix.ADDR_INET (Unix.inet_addr_any, 7890)) in
  let _ = Unix.listen server 1 in
  let _ = Unix.setsockopt_float server Unix.SO_RCVTIMEO 0.1 in
  let () = dprint "Starting Loop" in
  loop rooms server players []
