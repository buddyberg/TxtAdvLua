defmodule MUD.Server do
  def start(port, iex?) do
    IO.puts("Starting on port #{port}...")
    tcp_options = [:binary, packet: 0, active: false]
    Process.register(spawn(fn -> room(%{:desc => "This room is empty.\n", :players => [], :items => ["upworm"], :exits => %{"down" => :r1}}) end), :r0) #add exits
    Process.register(spawn(fn -> room(%{:desc => "This room is full.\n", :players => [], :items => ["beasts", "animals", "creatures", "monsters", "foes"], :exits => %{"up" => :r0}}) end), :r1)
    case :gen_tcp.listen(port, tcp_options) do
      {:ok, socket} -> listen(socket)
      {:error, :eaddrinuse} when iex? -> :eaddrinuse # this is OK in iex
      {:error, :eaddrinuse} -> start(port+1, iex?)
    end
  end
  defp listen(socket) do
    {:ok, conn} = :gen_tcp.accept(socket)
    :gen_tcp.send(conn, "Waiting for name...\n")
    spawn(fn -> naming(%{:conn => conn}) end)
    listen(socket)
  end

  defp naming(player) do
    case :gen_tcp.recv(player[:conn], 0) do
      {:ok, data} ->
        ndata = String.replace_suffix(data, "\n", "")
        :gen_tcp.send(player[:conn], ndata <> " has entered the world. Enjoy!\n")
        send(:r0,{:r0, ["newplayer", %{:name => ndata, :conn => player[:conn], :pid => self(), :inv => []}]})
        ploop(Map.put(Map.put(player, :name, ndata), :croom, :r0))
      {:error, :closed} ->
        :ok
    end
  end
  defp ploop(player) do
    conn = player[:conn]
    case :gen_tcp.recv(conn, 0) do
      {:ok, data} ->
        send(player[:croom], {self(), String.split(String.replace_suffix(data, "\n", ""))})
        receive do
          {nr, d2} -> nplayer = Map.update!(player, :croom, (fn x -> nr end)) #TODO: Update player
          	      IO.puts("Player sent " <> data <> "Sending back: \n" <> d2)
          	      :gen_tcp.send(conn, d2)
          	      ploop(nplayer)
          d2 -> IO.puts("Player sent " <> data <> "Sending back: \n" <> d2)
          	:gen_tcp.send(conn, d2)
          	ploop(player)
        end

      {:error, :closed} -> #TODO : cleanup
        :ok
    end
  end
  defp lhave([], _) do
    false
  end
  defp lhave(list, test) do
    case list do
    	[^test | _] -> true
    	[_ | tail] -> lhave(tail, test)
    end
  end
  defp sta([], _) do
    "OK.\n"
  end
  defp sta([player | rest], message) do
    :gen_tcp.send(player[:conn], message)
    sta(rest, message)
  end
  defp findplayer(rdata, pid) do
     (rdata[:players] |> Enum.filter(fn x -> x[:pid]==pid end) |> hd())
  end
  defp room(rdata) do
   receive do 
     {pid, ["look"]} -> istr = Enum.join(rdata[:items], "\n")
     			pstr = (rdata[:players] |> Enum.map((fn p -> p[:name] end)) |> Enum.join("\n"))
     			text = rdata[:desc]<>"\nItems in the room include:\n"<>istr<>"\n\nPlayers in the room include:\n"<>pstr<>"\n"
     			send(pid, text)
     			room(rdata)
     {pid, ["move", info]} -> case Map.has_key?(rdata[:exits], info) do
     			      true -> send(rdata[:exits][info], {pid, ["newplayer", findplayer(rdata, pid)]})
     			      	      nrdata = Map.update!(rdata, :players, (fn ps -> List.delete(ps, findplayer(rdata, pid)) end))
     			      	      send(pid, {rdata[:exits][info], "Moved! Send 'look' to look around.\n"})
     			      	      room(nrdata)
     			      false -> send(pid, "No such exit!\n")
     			      	       room(rdata)
     			      end
     {pid, ["inventory"]} -> inv = findplayer(rdata, pid)[:inv] |> Enum.intersperse("\n") |> List.to_string()
     			     send(pid, "--START--\n"<>inv<>"\n--END--\n")
     			     room(rdata)
     {pid, ["get", info]} -> case lhave(rdata[:items], info) do
     			     true -> pin = Enum.find_index(rdata[:players], (fn x -> (x[:pid]==pid) end))
     			     	     nrdata = update_in(rdata, [:players, Access.at(pin), :inv], (fn inv -> [info|inv] end))     			  
     			             nrdata = Map.update!(nrdata, :items, (fn x -> List.delete(x, info) end))
                             	     send(pid, "Picked up "<>info<>".\n") 
     			     	     room(nrdata)
     			     false -> send(pid, "No such item\n")
     			     	      room(rdata)
     			     end
     {pid, ["drop", info]} -> case lhave(findplayer(rdata, pid)[:inv], info) do
     				true -> pin = Enum.find_index(rdata[:players], (fn x -> (x[:pid]==pid) end))
     					nrdata = update_in(rdata, [:players, Access.at(pin), :inv], (fn inv -> List.delete(inv, info) end))
     					nrdata = Map.update!(nrdata, :items, (fn inv -> [info|inv] end))
     					send(pid, "Dropped "<>info<>".\n")
     			      		room(nrdata)
     			      	false -> send(pid, "You don't have this!\n")
     			      		 room(rdata)
     			     end
     {pid, ["say", info]} -> send(pid, sta(rdata[:players], hd(Enum.filter(rdata[:players],fn x-> x[:pid]==pid end))[:name]<>" said "<>info<>"\n"))
     			     room(rdata)
     {_pid, ["newplayer", info]} -> room(Map.put(rdata, :players, rdata[:players]++[info]))
     {pid, _} -> send(pid, "Invalid input\n")
                 room(rdata)
     _ -> room(rdata)
   end
  end
end

if !:erlang.function_exported(IEx, :started?, 0) do
  MUD.Server.start(6000, false)
end
